/**
 * Created by aideremir on 10.07.16.
 */
'use strict';

var hapGlobal = {
    app : {},
    current: {
        customer: null,
        account: null
    }
}

//Chart.defaults.global.colors = ['#fb6e04', '#cacaca'];

// Declare app level module which depends on views, and components
angular.module('hap', [
    'ngRoute', 'ngCookies', 'ngMaterial','chart.js'
]).config(['$routeProvider', 'ChartJsProvider', function ($routeProvider, ChartJsProvider) {

    //ChartJsProvider.setOptions({ colors : [ '#fb6e04'] });

    /*
    $routeProvider.when('/login', {
        templateUrl: 'views/login.html',
        controller: 'loginCtrl'
    }).when('/users', {
        templateUrl: 'views/users.html',
        controller: 'usersCtrl'
    }).when('/users/:id', {
        templateUrl: 'views/user.html',
        controller: 'userCtrl'
    }).otherwise({redirectTo: '/users'});
    */

}]).run(['$http', '$cookies', function run($http, $cookies) {


    if(!$cookies.get('userData')) {
        console.log('no token!!!');
        //window.location.href = '/login.html';
    }
    else {

        hapGlobal.app = $cookies.getObject('userData');

        $http.defaults.headers.common['Authorization'] = hapGlobal.app.token ? 'Bearer ' + hapGlobal.app.token : '';
        //window.location.href = '/index.html';
    }



}]);

function today() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    }

    if(mm<10) {
        mm='0'+mm
    }

    return yyyy + '-' + mm + '-' + dd;
}

function dateReformat(date) {

    var dd = date.getDate();
    var mm = date.getMonth()+1; //January is 0!
    var yyyy = date.getFullYear();

    if(dd<10) {
        dd='0'+dd
    }

    if(mm<10) {
        mm='0'+mm
    }

    return yyyy + '-' + mm + '-' + dd;
}