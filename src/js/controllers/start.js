/**
 * Created by aideremir on 10.07.16.
 */


angular.module('hap').controller('startCtrl', ['$scope', '$cookies', '$http', '$rootScope', function ($scope, $cookies, $http, $rootScope) {


    $scope.app = hapGlobal.app;
    $scope.current = hapGlobal.current;

    $scope.app.logout = function() {
        $http({
            method: 'POST',
            url: config.host + '/profile/sign_out',
        }).success(function() {

        });

        $cookies.remove('userData');
        $scope.app.token = null;
        hapGlobal.app.token = null;
        window.location.href = '/login.html';

    }



}]);