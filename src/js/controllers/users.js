/**
 * Created by aideremir on 10.07.16.
 */


angular.module('hap').controller('usersCtrl', ['$rootScope','$scope','$http', '$timeout', function ($rootScope, $scope, $http, $timeout) {

    $http.get(config.host + '/manager/customers').success(function (data) {
        $scope.users = data;
    });

     $scope.showUser = function(user) {
         hapGlobal.current.customer = user;
         $rootScope.$emit('showUser', user);
         //window.location.href = '/#/users/' + user.id;
     };

    $scope.customerSelectedClass = function(customer) {
        return hapGlobal.current.customer == customer ? 'hap-selected' : '';
    }
    
    
}]);