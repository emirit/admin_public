/**
 * Created by aideremir on 23.07.16.
 */

angular.module('hap').controller('chatCtrl', ['$rootScope','$scope','$http', '$timeout', function ($rootScope, $scope, $http, $timeout) {
    // chat ---------------- begin

    var userId = hapGlobal.app.user.id;
    var ws;
    var allMessages = {};
    var messages = {};
    var historyDates = {};
    var msgWrap = {};

    $scope.companion = 0;
    $scope.currUserName = 'Chat';
    $scope.messages = [];
    $scope.notViewed = {};
    $scope.typing = 0;
    $scope.historyDate = new Date();
    $scope.maxDate = new Date();

    $rootScope.$on('showUser', function(event, args) {
        //console.log(hapGlobal.current.customer);
        $scope.chatBegin(hapGlobal.current.customer);
    });

    $rootScope.$on('showAccount', function(event, args) {
        msgWrap = document.getElementById("msg-wrap");
        $scope.chatBegin(hapGlobal.current.customer);
    })

    $scope.getMessageClass = function (message) {
        return message.sender == userId ? 'my' : '';
    };

    $scope.activ = function (user) {
        return $scope.companion == user.id ? 'activ' : '';
    };

    $scope.getHistory = function () {
        historyDates[u($scope.companion)] = $scope.historyDate;
        ws.send(JSON.stringify({
            "type": "history",
            date: reformatDate($scope.historyDate),
            "companion": $scope.companion
        }));
    };

    $scope.chatBegin = function (user) {
        $scope.currUserName = user.name;
        $scope.companion = user.id;
        $scope.messages = messages[u(user.id)];
        $scope.historyDate = historyDates[u(user.id)] || new Date();
        $scope.maxDate = $scope.historyDate;
        if (!historyDates[u($scope.companion)]) {
            $scope.getHistory();
        }
        $timeout(function () {
            msgWrap.scrollTop = msgWrap.scrollHeight;
            if ($scope.messages) {
                $scope.messages.forEach(function (item) {
                    if (item.is_viewed == 0 && item.receiver == userId) {
                        ws.send(JSON.stringify({type: "viewMark", id: item.id}));
                    }
                });
            }
        }, 0);
    };

    $http.get(config.host + '/wskey').then(function successCallback(resp) {
        ws = new WebSocket('ws://' + resp.data.host + ':9000/' + resp.data.wskey);
        ws.onmessage = function (message) {
            var d = JSON.parse(message.data);
            if (d.type === 'unreviewedList') {
                d.data.forEach(function (item) {
                    addMsg(item);
                    var uId = u(getCompanionId(allMessages['id' + item.id]));
                    if(!historyDates[uId] || allMessages['id' + item.id].created_at < historyDates[uId]){
                        historyDates[uId] = allMessages['id' + item.id].created_at;
                    }
                });
                for(var _uId in historyDates){
                    var comp = _uId.substring(1);
                    ws.send(JSON.stringify({
                        "type": "history",
                        date: reformatDate(historyDates[_uId]),
                        "companion": comp
                    }));
                }
            } else if (d.type === 'message') {
                addMsg(d);
            } else if (d.type === 'notice') {
                console.log(d.content);
            }
            // else if (d.type === 'typing' || d.type === 'status') {
            //     $scope.typing = 1;
            //     if (promise) {
            //         $timeout.cancel(promise);
            //     }
            //     promise = $timeout(function () {
            //         $scope.typing = 0;
            //     }, 5000);
            // }
            else if (d.type === 'history') {
                d.data.forEach(function (item) {
                    addMsg(item);
                });
            } else if (d.type === 'viewMark') {
                allMessages['id' + d.id].is_viewed = 1;
                notViewedDec(u(getCompanionId(allMessages['id' + d.id])));
            }
            $scope.messages = messages[u($scope.companion)];
            $timeout(function () {
                $scope.$apply();
                msgWrap.scrollTop = msgWrap.scrollHeight;
            }, 0);
        };
    }, function errorCallback(response) {
        console.log(response);
    });

    $scope.sendMsg = function (messageText) {

        ws.send(JSON.stringify({
            type: 'message',
            receiver: $scope.companion + '',
            content: messageText
        }));
        $scope.messageText = '';
        messageText = '';

        document.getElementById('message-input').value = '';
    };

    $scope.sendStatus = function (msg) {
        ws.send(JSON.stringify(msg));
    };

    function u(id) {
        return 'u' + id;
    }

    $scope.u = function (id) {
        return u(id);
    };

    function addMsg(msg) {
        if (allMessages['id' + msg.id]) {
            return;
        }
        var rId = u(msg.receiver);
        var sId = u(msg.sender);
        msg.created_at = getDate(msg.created_at);
        allMessages['id' + msg.id] = msg;
        if (msg.sender == userId) {
            if (!messages[rId]) {
                messages[rId] = [];
            }
            messages[rId].push(msg);
        } else if (msg.receiver == userId) {
            if (!messages[sId]) {
                messages[sId] = [];
            }
            messages[sId].push(msg);
            if ($scope.companion != msg.sender && msg.is_viewed != 1) {
                notViewedInc(sId);
            }
        }
    }

    function reformatDate(dateObj) {
        return 'Y-m-d'
            .replace('Y', dateObj.getFullYear())
            .replace('m', ('0' + (dateObj.getMonth() + 1)).slice(-2))
            .replace('d', dateObj.getDate());
    }

    function notViewedInc(id) {
        if (!$scope.notViewed[id]) {
            $scope.notViewed[id] = 0;
        }
        ++$scope.notViewed[id];
    }

    function notViewedDec(id) {
        if (!$scope.notViewed[id] || $scope.notViewed[id] <= 0) {
            return
        }
        --$scope.notViewed[id];
    }

    function getDate(date) {
        date = date.replace(/-/g, "/").replace(/\.\d\d\dZ/g, "").replace(/T/g, " ");
        date = new Date(date + ' GMT');
        return date;
    }

    function getCompanionId(msg) {
        return msg.sender == userId ? msg.receiver : msg.sender;
    }



// chat --------------- end
}]);


