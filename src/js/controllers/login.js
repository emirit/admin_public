/**
 * Created by aideremir on 10.07.16.
 */


angular.module('hap').controller('loginCtrl', ['$scope', '$http', '$cookies', '$rootScope', '$mdToast', function ($scope, $http, $cookies, $rootScope, $mdToast) {

    var login = {
        submit: function () {

            if (!login.fields.email || !login.fields.password)
                return false;

            $http({
                method: 'POST',
                url: config.host + '/profile/sign_in',
                data: login.fields
            }).success(function (data) {

                if(data.token)
                {
                    $cookies.putObject('userData', data);

                    hapGlobal.app = {
                        token: data.token,
                        visits: data.visits
                    }
                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;

                    window.location.href = '/index.html';
                }
                else {
                    renderError('Incorrect username or password');
                }

            }).error(function () {
                renderError('Incorrect username or password');
            });
        }
    }

    $scope.loginForm = login;

    function renderError(msg) {

        $mdToast.show(
            $mdToast.simple()
                .textContent(msg)
                //.position(pinTo )
                .hideDelay(3000)
        );

    }


}]);