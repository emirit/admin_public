/**
 * Created by aideremir on 23.07.16.
 */


angular.module('hap').controller('productsCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {

    $rootScope.$on('showUser', function (event, args) {
        $scope.products = null;
    })

    $rootScope.$on('showAccount', function (event, args) {

        $scope.showLoader = true;
        $scope.products = null;
        $scope.accountId = args.id;

        $scope.isDateActive = function(date) {
            var d = dateReformat(date);

            //console.log(hapGlobal.current.customer.dates.indexOf(d));
            return hapGlobal.current.customer.dates.indexOf(d) >= 0;
        }

        getData(today());

    });

    $scope.onCalendarChange = function (dt) {

        $scope.showLoader = true;
        $scope.products = null;

        getData(dateReformat(dt));

    }



    function getData(date)
    {
        $http.get(config.host + '/accounts/' + $scope.accountId + '/products?date=' + date + '&lastVisitDate=' + date).success(function (data) {

            $scope.products = data;
            $scope.showLoader = false;
        });

    }


}]);
