/**
 * Created by aideremir on 23.07.16.
 */


angular.module('hap').controller('evolutionCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {

    $rootScope.$on('showUser', function (event, args) {
        $scope.account = null;
    })

    $rootScope.$on('showAccount', function (event, args) {

        $scope.showLoader = true;
        $scope.account = null;
        $scope.accountId = args.id;

        $scope.isDateActive = function(date) {
            var d = dateReformat(date);

            //console.log(hapGlobal.current.customer.dates.indexOf(d));
            return hapGlobal.current.customer.dates.indexOf(d) >= 0;
        }

        getData(today());

    });

    $scope.onCalendarChange = function (dt) {

        $scope.showLoader = true;
        $scope.account = null;

        getData(dateReformat(dt));

    }



    function getData(date)
    {
        $http.get(config.host + '/accounts/' + $scope.accountId + '?date=' + date).success(function (data) {

            $scope.account = data;
            $scope.showLoader = false;
        });

        $http.get(config.host + '/accounts/' + $scope.accountId + '/graph?dateEnd=' + date).success(function (data) {

            $scope.labels = [];
            $scope.data = [];

            for(var i=0; i < data.length; i++)
            {
                $scope.labels.push(data[i].date.substring(0,10));
                $scope.data.push(data[i].total);
            }

            $scope.series = ['Series A'];

            $scope.onClick = function (points, evt) {
                console.log(points, evt);
            };

            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
            $scope.options = {
                scales: {
                    yAxes: [
                        {
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left'
                        },
                        {
                            id: 'y-axis-2',
                            type: 'linear',
                            display: false,
                            position: 'right'
                        }
                    ]
                }
            };

        });
    }


}]);
