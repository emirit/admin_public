/**
 * Created by aideremir on 10.07.16.
 */


angular.module('hap').controller('portfolioCtrl', ['$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {

    $rootScope.$on('showUser', function(event, args) {

        $scope.showLoader = true;
        $scope.portfolio = null;


        $http.get(config.host + '/manager/portfolio/' + args.id + '?date=' + today()).success(function (data) {

            $scope.portfolio = data;
            $scope.showLoader = false;

            if(hapGlobal.current.customer != null)
                hapGlobal.current.customer.dates = data.dates;

            $scope.currency = (hapGlobal.current.customer != null) ? hapGlobal.current.customer.currency : '';
            var lastVisitDate = hapGlobal.current.customer.visits.pop();
            $scope.lastVisitDate = lastVisitDate ? lastVisitDate.visit_date : '';

        });

    });
    
    $scope.showAccount = function (account) {
        hapGlobal.current.account = account;
        $rootScope.$emit('showAccount', account);
    }

    $scope.accountSelectedClass = function(account) {
        return hapGlobal.current.account == account ? 'hap-selected' : '';
    }


}]);